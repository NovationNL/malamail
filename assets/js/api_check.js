function deepCheck(res, check) {
	if (typeof check == "object") {
		if (Array.isArray(check)) {
			for (let i = 0; i < check.length; i++) {
				if (!isEmpty(check[i]) && !isEmpty(res[i])) {
					const checkElement = check[i];
					const resElement = res[i]
					if (!deepCheck(resElement, checkElement)) return false
				} else {
					if (isEmpty(check[i]) && isEmpty(res[i])) continue
					return false
				}
			}
			return true
		} else {
			for (const key in check) {
				if (check.hasOwnProperty(key) && res.hasOwnProperty(key)) {
					const checkElement = check[key];
					const resElement = res[key];
					if (!deepCheck(resElement, checkElement)) return false
				} else {
					return false
				}
			}
			return true
		}
	} else {
		return (check === res)
	}
}

function getEmployersCheck(res, args) {
	console.debug(`[getEmployers] Always checks out when receiving status code 200.`)
	return true
}

function addEmployeeCheck(res, args) {
	let expectedResults
	switch (department) {
		case 'police':
			expectedResults = {
				callsigns: {
					default: "Agent",
					"Agent": {}
				},
				current_role: "Agent"
			}
			break;
		default:
			console.error(`[addEmployeeCheck] No department is set or department is invalid!`)
			break;
	}
	expectedResults.rp_name = args.rp_name
	expectedResults.rang = args.rang
	expectedResults.steam_id = args.steam_id
	expectedResults.callsigns[expectedResults.callsigns.default] = {
		callsign: args.callsign,
		identity: args.rp_name
	}
	expectedResults.main_callsign = parseInt(args.callsign.replace("-", ""))
	expectedResults.specials = []
	expectedResults.online = false
	expectedResults.available = false
	expectedResults.current_role = null
}
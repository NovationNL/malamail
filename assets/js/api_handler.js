// API Handlers
function listAPIEndpoints() {
  console.log(`API Endpoints (all function have to be awaited in a async parent!):
  Employers API:
    GET - getEmployers()
    POST - addEmployee(rp_name, rang, steam_id, callsign)
    POST - findEmployeeBySteamID(steamID)
    POST - editEmployee(steamID, field, value, innerField = null)
    POST - deleteEmployee(steamID)
  Squad API:
    GET - getSquads()
    POST - createSquad(steam_ids, leader_steam_id)
    POST - getSquadUIDBySteamID(steam_ids)
    POST - setSquad(steam_ids, leader_steam_id)
    POST - addSquadMember(steam_id, group_id)
    POST - removeSquadMember(steam_id, group_id)
    POST - setSquadLeader(steam_id, group_id)
    POST - deleteSquad(group_id)
  Emergency API:
    GET - getEmergencies()
    POST - addEmergency(steam_ids)
    POST - deleteEmergency(emergency_id)
    POST - addEmployeeToEmergency(emergency_id, steam_id)
    POST - removeEmployeeFromEmergency(emergency_id, steam_id)
    POST - addMultipleEmployeesToEmergency(emergency_id, steam_ids)
    POST - saveEmergency(emergency_id, title, unitsOnLocation, notes)
  Local Tools API:
    getCurrentCallsign(employee)
    getSquadIndexBySteamID(steam_id, groups)
    ^^ 'groups' argument is optional but preferred ^^
    getEmergencyIndexBySteamID(groupID, emergencies)
    ^^ 'emergencies' argument is optional but preferred ^^
    getSquadIndexByUID(steam_id, groups)
    ^^ 'groups' argument is optional but preferred ^^
    printSquadMemebers(squadMates, useLightColors = false)
`)
}

// Employee API
function getEmployers() {
  return $.ajax({
    type: 'GET',
    url: `/api/getEmployers?department=${department}`
  })
}

function addEmployee(rp_name, rang, steam_id, callsign) {
  return $.ajax({
    type: 'POST',
    url: `/api/addEmployee?department=${department}`,
    data: {
      rp_name,
      rang,
      steam_id,
      callsign
    }
  })
}

function findEmployeeBySteamID(steamID) {
  return $.ajax({
    type: 'POST',
    url: `/api/findEmployee?by=steam_id&department=${department}`,
    data: {
      value: steamID
    }
  })
}

function findSteamIDByCallsign(callsign) {
  return $.ajax({
    type: 'POST',
    url: `/api/findSteamIDByCallSign?department=${department}`,
    data: {
      callsign
    }
  })
}

// innerField: is optional (depends on the field)
function editEmployee(steamID, field, value, innerField = null) {
  return $.ajax({
    type: 'POST',
    url: `/api/editEmployeeField?department=${department}&steam_id=${steamID}`,
    data: {
      field,
      value,
      innerField
    }
  })
}

function deleteEmployee(steam_id) {
  return $.ajax({
    type: 'POST',
    url: `/api/deleteEmployee?department=${department}`,
    data: {
      steam_id
    }
  })
}

// Squad API
// steam_ids: Must be an array with steam IDs
function createSquad(steam_ids, leader_steam_id) {
  return setSquad(steam_ids, leader_steam_id)
}

// steam_ids: Must be an array with steam IDs
function setSquad(steam_ids, leader_steam_id) {
  return $.ajax({
    type: 'POST',
    url: `/api/setSquad?department=${department}`,
    data: {
      steam_ids: JSON.stringify(steam_ids),
      leader_steam_id
    }
  })
}

function getSquads() {
  return $.ajax({
    type: 'GET',
    url: `/api/getSquads?department=${department}`
  })
}

function getSquadBySteamID(steam_id) {
  return $.ajax({
    type: 'POST',
    url: `/api/getSquadBySteamID?department=${department}`,
    data: {
      steam_id
    }
  })
}

function getSquadUIDBySteamID(steam_id) {
  return $.ajax({
    type: 'POST',
    url: `/api/getSquadUIDBySteamID?department=${department}`,
    data: {
      steam_id
    }
  })
}

function getSquadUIDByIndex(index) {
  return $.ajax({
    type: 'POST',
    url: `/api/getSquadUIDByIndex?department=${department}`,
    data: {
      index
    }
  })
}

function addSquadMember(steam_id, group_id) {
  return $.ajax({
    type: 'POST',
    url: `/api/addSquadMember?department=${department}`,
    data: {
      steam_id,
      group_id
    }
  })
}

function addMultipleEmployeesToSquad(steam_ids, group_id) {
  return $.ajax({
    type: 'POST',
    url: `/api/addMultipleEmployeesToSquad?department=${department}`,
    data: {
      steam_ids,
      group_id
    }
  })
}

function removeSquadMember(steam_id, group_id) {
  return $.ajax({
    type: 'POST',
    url: `/api/removeSquadMember?department=${department}`,
    data: {
      steam_id,
      group_id
    }
  })
}

function setSquadLeader(steam_id, group_id) {
  return $.ajax({
    type: 'POST',
    url: `/api/setSquadLeader?department=${department}`,
    data: {
      steam_id,
      group_id
    }
  })
}

function deleteSquad(group_id) {
  return $.ajax({
    type: 'POST',
    url: `/api/deleteSquad?department=${department}`,
    data: {
      group_id
    }
  })
}

// Emergency API
function getEmergencies() {
  return $.ajax({
    type: 'GET',
    url: `/api/getEmergencies?department=${department}`
  })
}

function getEmergencyBySteamID(steam_id) {
  return $.ajax({
    type: 'GET',
    url: `/api/getEmergencyBySteamID?department=${department}&steam_id=${steam_id}`
  })
}

// steam_ids: Must be an array with steam IDs
function addEmergency(steam_ids) {
  return $.ajax({
    type: 'POST',
    url: `/api/addEmergency?department=${department}`,
    data: {
      steam_ids: JSON.stringify(steam_ids)
    }
  })
}

function deleteEmergency(emergency_id) {
  return $.ajax({
    type: 'POST',
    url: `/api/deleteEmergency?department=${department}`,
    data: {
      emergency_id
    }
  })
}

function addEmployeeToEmergency(emergency_id, steam_id) {
  return $.ajax({
    type: 'POST',
    url: `/api/addEmployeeToEmergency?department=${department}`,
    data: {
      emergency_id,
      steam_id
    }
  })
}

function removeEmployeeFromEmergency(emergency_id, steam_id) {
  return $.ajax({
    type: 'POST',
    url: `/api/removeEmployeeFromEmergency?department=${department}`,
    data: {
      emergency_id,
      steam_id
    }
  })
}

// steam_ids: Must be an array with steam IDs
function addMultipleEmployeesToEmergency(emergency_id, steam_ids) {
  return $.ajax({
    type: 'POST',
    url: `/api/addMultipleEmployeesToEmergency?department=${department}`,
    data: {
      emergency_id,
      steam_ids: JSON.stringify(steam_ids)
    }
  })
}

function saveEmergency(emergency_id, title, notes) {
  return $.ajax({
    type: 'POST',
    url: `/api/saveEmergency?department=${department}`,
    data: {
      emergency_id,
      title,
      notes
    }
  })
}
